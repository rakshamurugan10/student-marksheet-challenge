let items=require('../items')
// Separate handler function for each http request 
const getItems=function(req,res) {
    res.send(items);
}

const addItem=function(req,res)
{    
    
    const{studentName , studentID,subject1,subject2,subject3,subject4,subject5}=req.body
    const item={

        studentName,
        studentID,
        subject1,
        subject2,
        subject3,
        subject4,
        subject5
    }
    items=[...items,item]
    res.code(201).send(items)
    
} 

const deleteItem=function(req,res){
    
    items=items.filter(item => item.studentID !==req.body.studentID)
    res.send({message:"Successfully deleted"})
}

const updateItem=function(req,res){
    
    const{studentName ,studentID, subject1,subject2,subject3,subject4,subject5}=req.body

    items=items.map(item =>(item.studentID=== studentID ?{studentID,studentName,subject1,subject2,subject3,subject4,subject5}:item))
    res.send(items)

    
}
module.exports={getItems,addItem,deleteItem,updateItem}