const {getItems,addItem,deleteItem,updateItem}=require('../controller.js/handler')
//Item Schema

 const Item={
    type:'object',
     properties:{
        studentName:{type:'string'},
        studentID:{type:'string'},
        subject1:{type:'string'},
        subject2:{type:'string'},
        subject3:{type:'string'},
        subject4:{type:'string'},
        subject5:{type:'string'}


    },
}

//validation schema 
const getStudentsList={
    schema:{
        response:{
            200:{
                type:'array',
                items:Item
            },
        },
   
   
    },
    handler:getItems
    
}

const addStudent={
    schema:{
        body:{
            type:'object',
            required:['studentName','studentID','subject1'],
            properties:{
                studentName:{type:'string'},
                studentID:{type:'string'},
                subject1:{type:'string'}
            },
        },
        response:{
            201:{
                type:'array',
                items:Item
            },

            
        },
    },
   handler:addItem
}

const deleteStudent={
    schema:{
        response:{
            200:{
                type:'object',
                properties:{
                    message:{type: 'string'}
                }
            }
        }
    },
    handler:deleteItem
}

const updateStudent={
    schema:{
        response:{
            200:{
                type:'array',
                items:Item
            },
        },
    },
    handler:updateItem
}

function studentRoutes(fastify,options,done)
{  //Get All items
    fastify.get('/report',getStudentsList) 

    //Add item 
    fastify.post('/add', addStudent)

    //Delete item
    fastify.delete('/delete', deleteStudent)
    
    //Update item
    fastify.put('/update',updateStudent)

    done()
}

module.exports=studentRoutes